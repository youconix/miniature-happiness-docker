#  Miniature Happiness development image
Miniature-happiness-docker is a set of Docker-images designed to develop websites on the Miniature-hapiness framework without worrying about all the needed dependancies.
This image also isolates the requirements from other websites in development.


## Installation
This image requires docker te be installed and that docker is usable from the commandline without using sudo.  
Before building the image make sure that your website code is checked out in apps/mh.  
You are free to change the directory name mh as long as you it also change in docker-compose.overide.yml.

Run the following command to build the image:

```bash
make -f bin/Makefile build
```

## Available commands in the Makefile

The following commands are declared in the make file

| Command || Description |
| :--- | --- | :--- |
| bash || Loads a bash shell for the PHP container |
| build || (Re)Builds the containers |
| build-resources || Builds the JavaScript and CSS with Webpack |
| fixtures || (Re)Loads the fixtures. This destroys all data in the database! |
| getver || Shows the current git commit |
| init || Installs composer, builds all the resources and resets the database |
| reset-database || Drops and recreates the database. This destroys all data in the database! |
| setup-database || Alias or reset-database and fixtures |
| start || Alias of docker-compose up |
| stop || Alias of docker-compose down |
| update-composer || Updates composer.json and composer.lock to the newest versions |
| upgrade-composer || Upgrades the composer runtime to a new version |


## Available values in the .env file

The following values are declared in the environment file. Feel free to change them to your liking.  
Keep in mind that the database connection values need to be changed before the first build & run.  
__DO NOT__ use these credentials in production!  

| Value || Default value || Description |
| :--- | --- | :--- | --- | :--- |
| MYSQL_ROOT_PASSWORD || I-have-the-power || Database root password |
| MYSQL_DATABASE || application_database || Database name |
| MYSQL_USER || application_user || Database user |
| MYSQL_PASSWORD || application_password || Database password |
| MYSQL_PORT || 3306 || External database port |
| || || |
| NGINX_PORT || 80 || External http port |
| NGINX_SSL_PORT || 443 || External https port |
| || || |
| LOCAL_USER || 1000:1000 || User and group for the PHP image. Corresponds with first user on a Linux host |

## Using as base image
Feel free to use this image as your own development base image.  
Just don't forget to check for upstream updates

The PHP modules can be found in _php/Dockerfile_

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

## License
[GPL 3](https://www.gnu.org/licenses/gpl-3.0.en.html)
